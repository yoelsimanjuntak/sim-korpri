<?php
$this->load->model('mpost');
$ruser = GetLoggedUser();
$docs = $this->mpost->search(0,"",3);
 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $rpegawai = $this->db->where("YEAR(CURDATE()) - YEAR(PegTglLahir) < PegBUP")->count_all_results(TBL_MPEGAWAI);
        $rskpd = $this->db->count_all_results(TBL_MSKPD);
        $rkas = $this->db->select("sum(KasJumlah) as Total")->where(COL_ISVERIFIED, 1)->get(TBL_TKAS)->row_array();
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($rskpd)?></h3>

              <p class="font-italic">SKPD</p>
            </div>
            <div class="icon">
              <i class="fas fa-institution"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=number_format($rpegawai)?></h3>

              <p class="font-italic">PEGAWAI AKTIF</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($rkas['Total'])?></h3>

              <p class="font-italic">JUMLAH KAS</p>
            </div>
            <div class="icon">
              <i class="fas fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      } else {
        $idSKPD = $ruser[COL_COMPANYID];
        $rpegawai = $this->db->where(COL_IDSKPD, $idSKPD)->where("YEAR(CURDATE()) - YEAR(PegTglLahir) < PegBUP")->count_all_results(TBL_MPEGAWAI);
        $rkasmasuk = $this->db->select("sum(KasJumlah) as Total")->where(COL_IDSKPD, $idSKPD)->where(COL_KASTIPE, 'MASUK')->where(COL_ISVERIFIED, 1)->get(TBL_TKAS)->row_array();
        $rkaskeluar = $this->db->select("sum(KasJumlah*(-1)) as Total")->where(COL_IDSKPD, $idSKPD)->where(COL_KASTIPE, 'KELUAR')->where(COL_ISVERIFIED, 1)->get(TBL_TKAS)->row_array();
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=number_format($rpegawai)?></h3>

              <p class="font-italic">PEGAWAI</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($rkasmasuk['Total'])?></h3>

              <p class="font-italic">TOTAL IURAN</p>
            </div>
            <div class="icon">
              <i class="fas fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($rkaskeluar['Total'])?></h3>

              <p class="font-italic">TOTAL SANTUNAN</p>
            </div>
            <div class="icon">
              <i class="fas fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      }
      ?>

      <?php
      $idSKPD = "";
      $condSkpd = " 1=1 ";
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd .= "and IdSkpd = ".$ruser[COL_COMPANYID];
        $idSKPD = $ruser[COL_COMPANYID];
      }
      $rpegawai_ = $this->db
      ->where("MONTH(CURDATE()) = MONTH(PegTglLahir) and YEAR(CURDATE()) - YEAR(PegTglLahir) = PegBUP and ".$condSkpd)
      ->count_all_results(TBL_MPEGAWAI);

      $rjmliuran_ = $this->db
      ->select("SUM(COALESCE(mgolongan.GolonganJmlIuran, 0) + COALESCE(mjabatan.JabJmlIuran, 0)) as Total")
      ->join(TBL_MGOLONGAN,TBL_MGOLONGAN.'.'.COL_GOLONGANKODE." = ".TBL_MPEGAWAI.".".COL_PEGGOLONGAN,"left")
      ->join(TBL_MJABATAN,TBL_MJABATAN.'.'.COL_JABNAMA." = ".TBL_MPEGAWAI.".".COL_PEGJABATAN,"left")
      ->where("DATE_ADD(PegTglLahir, INTERVAL PegBUP YEAR) >= CURDATE() and ".$condSkpd)
      ->get(TBL_MPEGAWAI)
      ->row_array();

      $rkategori_ = $this->db
      ->where(COL_KATNAMA, 'PNS Pensiun')
      ->get(TBL_MKATEGORI)
      ->row_array();

      $kas_ = 0;
      $kas_ += !empty($rjmliuran_)?$rjmliuran_['Total']:0;
      $kas_ -= (!empty($rkategori_)?$rkategori_[COL_KATJUMLAH]:0)*$rpegawai_;
      ?>
      <div class="col-lg-8">
        <div class="card card-navy">
          <div class="card-header">
            <h5 class="card-title">PROYEKSI PERIODE <strong><?=date('m-Y')?></strong></h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>PROYEKSI JLH. PEGAWAI PENSIUN</td>
                  <td class="text-center" style="width: 10px !important">:</td>
                  <td class="text-right font-weight-bold">
                    <a href="<?=site_url('site/laporan/pensiun')."?IdSkpd=".(!empty($idOPD)?$idOPD:'')."&Bulan=".date('m')."&Tahun=".date('Y')?>">
                      <?=number_format($rpegawai_)?>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>PROYEKSI JLH. IURAN</td>
                  <td class="text-center" style="width: 10px !important">:</td>
                  <td class="text-right font-weight-bold">
                    <a href="<?=site_url('site/laporan/iuran')."?IdSkpd=".(!empty($idOPD)?$idOPD:'')."&Bulan=".date('m')."&Tahun=".date('Y')?>">
                      <?=number_format(!empty($rjmliuran_)?$rjmliuran_['Total']:0)?>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>PROYEKSI JLH. SANTUNAN PENSIUN</td>
                  <td class="text-center" style="width: 10px !important">:</td>
                  <td class="text-right font-weight-bold"><?=number_format((!empty($rkategori_)?$rkategori_[COL_KATJUMLAH]:0)*$rpegawai_)?></td>
                </tr>
                <tr>
                  <td>PERTAMBAHAN KAS</td>
                  <td class="text-center" style="width: 10px !important">:</td>
                  <td class="text-right font-weight-bold"><?=number_format($kas_)?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card card-navy">
          <div class="card-header">
            <h5 class="card-title">INFORMASI</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <?php
                if(!empty($docs)) {
                  foreach($docs as $d) {
                    $rfile = $this->db
                    ->where(COL_POSTID, $d[COL_POSTID])
                    ->get(TBL__POSTIMAGES)
                    ->result_array();
                    ?>
                    <tr>
                      <td><a target="_blank" href="<?=!empty($rfile)?MY_UPLOADURL.$rfile[0][COL_IMGPATH]:''?>"><?=$d[COL_POSTTITLE]?></a></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
</script>
