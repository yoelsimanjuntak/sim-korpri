<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <form action="<?=current_url()?>" method="get">
              <table class="table table-bordered mb-0">
                <thead>
                  <tr>
                    <td>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">SKPD :</label>
                        <div class="col-lg-10">
                          <?php
                          $ruser = GetLoggedUser();
                          if($ruser[COL_ROLEID] != ROLEADMIN) {
                            $rskpd = $this->db
                            ->where(COL_SKPDID, $ruser[COL_COMPANYID])
                            ->get(TBL_MSKPD)
                            ->row_array();
                            ?>
                            <input type="hidden" name="IdSkpd" value="<?=$ruser[COL_COMPANYID]?>" />
                            <input type="text" class="form-control" value="<?=!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-'?>" readonly />
                            <?php
                          } else {
                            ?>
                            <select class="form-control" name="IdSkpd" style="width: 100%">
                              <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $getSKPD, true, false, '-- SEMUA SKPD --')?>
                            </select>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label class="control-label col-lg-2">PERIODE :</label>
                        <div class="col-lg-2">
                          <select class="form-control" name="Bulan" style="width: 100%">
                            <option value="1" <?=$getBulan==1?'selected':''?>>JANUARI</option>
                            <option value="2" <?=$getBulan==2?'selected':''?>>FEBRUARI</option>
                            <option value="3" <?=$getBulan==3?'selected':''?>>MARET</option>
                            <option value="4" <?=$getBulan==4?'selected':''?>>APRIL</option>
                            <option value="5" <?=$getBulan==5?'selected':''?>>MEI</option>
                            <option value="6" <?=$getBulan==6?'selected':''?>>JUNI</option>
                            <option value="7" <?=$getBulan==7?'selected':''?>>JULI</option>
                            <option value="8" <?=$getBulan==8?'selected':''?>>AGUSTUS</option>
                            <option value="9" <?=$getBulan==9?'selected':''?>>SEPTEMBER</option>
                            <option value="10" <?=$getBulan==10?'selected':''?>>OKTOBER</option>
                            <option value="11" <?=$getBulan==11?'selected':''?>>NOVEMBER</option>
                            <option value="12" <?=$getBulan==12?'selected':''?>>DESEMBER</option>
                          </select>
                        </div>
                        <div class="col-lg-2">
                          <select class="form-control" name="Tahun" style="width: 100%">
                            <?php
                            for($i=date('Y'); $i<=date('Y')+5; $i++) {
                              ?>
                              <option value="<?=$i?>" <?=$getTahun==$i?'selected':''?>><?=$i?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </td>
                  </tr>
                </thead>
              </table>
            </form>

          </div>
        </div>
        <?php
        if(isset($res)) {
          ?>
          <div class="card">
            <div class="card-body p-0">
              <table class="table table-bordered w-100">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th style="width: 10px; white-space: nowrap">GOL. / ESELON</th>
                    <th>SKPD</th>
                    <th>IURAN</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  $no=1;
                  if(!empty($res)) {
                    foreach($res as $r) {
                      ?>
                      <tr>
                        <td class="text-right" style="width: 10px; white-space: nowrap"><?=$no?></td>
                        <td><?=$r[COL_PEGNAMA]?><br /><small>NIP. <?=$r[COL_PEGNIP]?></small></td>
                        <td style="width: 10px; white-space: nowrap"><?=$r[COL_PEGGOLONGAN].(!empty($r[COL_PEGJABATAN])?' <br /><small>'.$r[COL_PEGJABATAN].'</small>':'')?></td>
                        <td><?=$r[COL_SKPDNAMA]?></td>
                        <td class="text-right"><?=number_format($r['Iuran'])?></td>
                      </tr>
                      <?php
                      $no++;
                      $sum+=$r['Iuran'];
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5" class="text-center font-italic">(KOSONG)</td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="4" class="text-center font-weight-bold">TOTAL</td>
                    <td class="text-right font-weight-bold"><?=number_format($sum)?></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select', $('form')).change(function(){
    $(this).closest('form').submit();
  });
});
</script>
