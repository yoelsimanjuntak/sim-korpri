<?php
$ruser = GetLoggedUser();
if($ruser[COL_ROLEID]==ROLEADMIN) {
  ?>
  <div class="form-group">
    <label>SKPD</label>
    <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%">
      <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, (!empty($data)?$data[COL_IDSKPD]:''), true, false, '-- PILIH SKPD --')?>
    </select>
  </div>
  <?php
}
?>

<div class="form-group">
  <div class="row">
    <div class="col-sm-6">
      <label>NAMA</label>
      <input type="text" class="form-control" placeholder="Nama Lengkap" name="<?=COL_PEGNAMA?>" value="<?=!empty($data)?$data[COL_PEGNAMA]:''?>" />
    </div>
    <div class="col-sm-6">
      <label>NIP</label>
      <input type="text" class="form-control" placeholder="NIP" name="<?=COL_PEGNIP?>" value="<?=!empty($data)?$data[COL_PEGNIP]:''?>" />
    </div>
  </div>
</div>
<div class="form-group">
  <div class="row">
    <div class="col-sm-3">
      <label>Tanggal Lahir</label>
      <input type="text" class="form-control datepicker" name="<?=COL_PEGTGLLAHIR?>" value="<?=!empty($data)?$data[COL_PEGTGLLAHIR]:''?>" />
    </div>
    <div class="col-sm-3">
      <label>TMT PNS</label>
      <input type="text" class="form-control datepicker" name="<?=COL_PEGTMT?>" value="<?=!empty($data)?$data[COL_PEGTMT]:''?>" />
    </div>
    <div class="col-sm-4">
      <label>Batas Usia Pensiun (BUP)</label>
      <input type="number" class="form-control" placeholder="BUP" name="<?=COL_PEGBUP?>" value="<?=!empty($data)?$data[COL_PEGBUP]:''?>" />
    </div>
  </div>
</div>
<div class="form-group">
  <div class="row">
    <div class="col-sm-6">
      <label>Golongan</label>
      <select class="form-control" name="<?=COL_PEGGOLONGAN?>" style="width: 100%">
        <?=GetCombobox("select * from mgolongan order by GolonganSeq", COL_GOLONGANKODE, array(COL_GOLONGANKODE, COL_GOLONGANNAMA), (!empty($data)?$data[COL_PEGGOLONGAN]:''), true, false, '-- PILIH GOLONGAN --')?>
      </select>
    </div>
    <div class="col-sm-6">
      <label>Jabatan</label>
      <select class="form-control" name="<?=COL_PEGJABATAN?>" style="width: 100%">
        <?=GetCombobox("select * from mjabatan order by JabSeq", COL_JABNAMA, COL_JABNAMA, (!empty($data)?$data[COL_PEGJABATAN]:''), true, false, 'TIDAK ADA')?>
      </select>
    </div>
  </div>
</div>
<div class="form-group">
  <label>UNGGAH SK</label>
  <div id="div-attachment">
    <div class="input-group mb-2">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
      </div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
        <label class="custom-file-label" for="file">PILIH FILE</label>
      </div>
    </div>
    <?php
    if(!empty($data)&&!empty($data[COL_PEGFILESK])) {
      if(file_exists(MY_UPLOADPATH.$data[COL_PEGFILESK])) {
        ?>
        <ul class="todo-list ui-sortable mb-2" data-widget="todo-list">
          <li>
            <div class="d-inline mr-2">
              <i class="far fa-paperclip"></i>
            </div>
            <span class="text font-italic"><?=$data[COL_PEGFILESK]?></span>
            <div class="tools">
              <a href="javascript:window.open('<?=MY_UPLOADURL.$data[COL_PEGFILESK]?>')"><i class="far fa-download"></i></a>
            </div>
          </li>
        </ul>
        <?php
      }
    }
    ?>
    <p class="text-sm text-muted mb-0 font-italic">
      <strong>CATATAN:</strong><br />
      - Besar file / dokumen maksimum <strong>5 MB</strong><br />
      - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>, <strong>PDF</strong>, <strong>DOC / DOCX</strong>, <strong>XLS / XLSX</strong>
    </p>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
});
</script>
