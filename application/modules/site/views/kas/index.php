<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
label.error[for=file] {
  display: none !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/kas/add/'.$tipe)?>" type="button" class="btn btn-tool btn-add text-primary"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">AKSI</th>
                    <!--<th>SKPD</th>-->
                    <th>TANGGAL</th>
                    <th>KETERANGAN</th>
                    <th>JUMLAH</th>
                    <th>STATUS</th>
                    <th>DIINPUT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">ENTRI KAS <strong><?=strtoupper($tipe)?></strong></span>
      </div>
      <form id="form-doc" action="<?=current_url()?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" id="btn-save" class="btn btn-sm btn-success"><i class="far fa-arrow-circle-right"></i>&nbsp;SIMPAN</button>
          <button type="submit" id="btn-verif" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i>&nbsp;VERIFIKASI</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <?php
  $ruser = GetLoggedUser();
  if($ruser[COL_ROLEID] != ROLEADMIN) {
    $rskpd = $this->db
    ->where(COL_SKPDID, $ruser[COL_COMPANYID])
    ->get(TBL_MSKPD)
    ->row_array();
    ?>
    <input type="hidden" name="IdSkpd" value="<?=$ruser[COL_COMPANYID]?>" />
    <input type="text" class="form-control" value="<?=!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-'?>" style="width: 60vw !important" readonly />
    <?php
  } else {
    ?>
    <select class="form-control" name="filterSkpd" style="min-width: 300px">
      <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, null, true, false, '-- SEMUA SKPD --')?>
    </select>
    <?php
  }
  ?>

</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/kas/index-load/'.$tipe)?>",
      "type": 'POST',
      "data": function(data){
        data.filterSkpd = $('[name=filterSkpd]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "PENCARIAN "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row pt-3'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [[ 1, "desc" ]],
    "columnDefs": [
      {"targets":[0,4], "className":'nowrap text-center'},
      {"targets":[1,3,5], "className":'nowrap dt-body-right'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      //{"orderable": true},
      {"orderable": true,"width": "100px"},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false,"width": "100px"},
      {"orderable": true,"width": "10px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-modal', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', $('#modal-form')).empty();
        $('.modal-body', $('#modal-form')).load(url, function(){
          $('#form-doc', $('#modal-form')).attr('action', url);
          $("select", $('#modal-form')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
          $('#modal-form').modal('show');
        });
        return false;
      });
      $('[data-toggle="tooltip"]', $(row)).tooltip();
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Kata Kunci');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', $('#modal-form')).empty();
    $('.modal-body', $('#modal-form')).load(url, function(){
      $('#form-doc', $('#modal-form')).attr('action', url);
      $("select", $('#modal-form')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('#modal-form').modal('show');
    });
    return false;
  });

  $('#form-doc').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      //btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            dt.DataTable().ajax.reload();
            $('#modal-form').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          //btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
