<?php
$ruser = GetLoggedUser();
if($ruser[COL_ROLEID]==ROLEADMIN) {
  ?>
  <div class="form-group">
    <label>SKPD</label>
    <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%">
      <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, (!empty($data)?$data[COL_IDSKPD]:''), true, false, '-- PILIH SKPD --')?>
    </select>
  </div>
  <?php
}
?>
<input type="hidden" name="<?=COL_ISVERIFIED?>" value="<?=!empty($data)?$data[COL_ISVERIFIED]:''?>" />
<?php
if($tipe=='keluar') {
  ?>
  <div class="form-group">
    <label>PEGAWAI</label>
    <select class="form-control" name="<?=COL_IDPEGAWAI?>" style="width: 100%">
      <?php
      if(!empty($data)) {
        echo GetCombobox("select * from mpegawai where IdSkpd=".$data[COL_IDSKPD]." order by PegNama", COL_PEGID, array(COL_PEGNAMA, COL_PEGNIP), $data[COL_IDPEGAWAI], true, false, '-- PILIH PEGAWAI --');
      } else {
        if($ruser[COL_ROLEID] != ROLEADMIN) {
          echo GetCombobox("select * from mpegawai where IdSkpd=".$ruser[COL_COMPANYID]." order by PegNama", COL_PEGID, array(COL_PEGNAMA, COL_PEGNIP), null, true, false, '-- PILIH PEGAWAI --');
        } else {
          ?>
          <option>-- PILIH PEGAWAI --</option>
          <?php
        }

      }
      ?>
    </select>
  </div>
  <?php
}
?>
<div class="form-group">
  <div class="row">
    <div class="col-sm-3">
      <label>TANGGAL</label>
      <input type="text" class="form-control datepicker" name="<?=COL_KASTANGGAL?>" value="<?=!empty($data)?$data[COL_KASTANGGAL]:''?>" />
    </div>
    <div class="col-sm-3">
      <label>JUMLAH</label>
      <input type="text" class="form-control uang text-right" placeholder="Rp." name="<?=COL_KASJUMLAH?>" value="<?=!empty($data)?($data[COL_KASTIPE]=='MASUK'?$data[COL_KASJUMLAH]:$data[COL_KASJUMLAH]*-1):''?>" />
    </div>
    <?php
    if($tipe=='keluar') {
      ?>
      <div class="col-sm-6">
        <label>JENIS</label>
        <select class="form-control" name="<?=COL_KASKATEGORI?>" style="width: 100%">
          <?=GetCombobox("select * from mkategori", COL_KATNAMA, COL_KATNAMA, (!empty($data)?$data[COL_KASKATEGORI]:''))?>
        </select>
      </div>
      <?php
    }
    ?>
  </div>
</div>
<div class="form-group">
  <label>KETERANGAN</label>
  <textarea class="form-control" rows="2" placeholder="Keterangan / Rincian / Catatan Tambahan" name="<?=COL_KASKETERANGAN?>"><?=!empty($data)?$data[COL_KASKETERANGAN]:''?></textarea>
</div>
<div class="form-group">
  <label>LAMPIRAN</label>
  <div id="div-attachment">
    <div class="input-group mb-2">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
      </div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
        <label class="custom-file-label" for="file">PILIH FILE</label>
      </div>
    </div>
    <?php
    if(!empty($data)&&!empty($data[COL_KASFILE])) {
      if(file_exists(MY_UPLOADPATH.$data[COL_KASFILE])) {
        ?>
        <ul class="todo-list ui-sortable mb-2" data-widget="todo-list">
          <li>
            <div class="d-inline mr-2">
              <i class="far fa-paperclip"></i>
            </div>
            <span class="text font-italic"><?=$data[COL_KASFILE]?></span>
            <div class="tools">
              <a href="javascript:window.open('<?=MY_UPLOADURL.$data[COL_KASFILE]?>')"><i class="far fa-download"></i></a>
            </div>
          </li>
        </ul>
        <?php
      }
    }
    ?>
    <p class="text-sm text-muted mb-0 font-italic">
      <strong>CATATAN:</strong><br />
      - Besar file / dokumen maksimum <strong>5 MB</strong><br />
      - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>, <strong>PDF</strong>, <strong>DOC / DOCX</strong>, <strong>XLS / XLSX</strong>
    </p>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $(document).on('blur','.uang',function(){
      $(this).val(desimal($(this).val(),0));
  }).on('focus','.uang',function(){
      $(this).val(toNum($(this).val()));
  });
  $(".uang").trigger("blur");
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=IdSkpd]').change(function(){
    var _this = $(this);
    var _id = $(this).val();
    if(_id) {
      _this.attr('disabled', true);
      $('[name=IdPegawai]').load('<?=site_url('site/ajax/load-opt-pegawai')?>', {IdSkpd: _id},function(){
        _this.attr('disabled', false);
      });
    }
  });

  <?php
  if(empty($data) && $ruser[COL_ROLEID] != ROLEADMIN) {
    ?>
    $('[name=IdSkpd]').trigger('change');
    <?php
  }
  ?>

  $('[name=KasKategori]').change(function(){
    var _this = $(this);
    var _id = $(this).val();
    if(_id) {
      $.get('<?=site_url('site/ajax/get-jumlah-santunan')?>', {KasKategori: _id}, function(jml) {
        if(jml) {
          $('[name=KasJumlah]').val(jml).attr('readonly', true).blur();
        }
      });
    }
  }).trigger('change');

  <?php
  if(!empty($data)) {
    if($data[COL_ISVERIFIED]==0) {
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        ?>
        $('button#btn-verif[type=submit]', $('#form-doc')).show();
        <?php
      } else {
        ?>
        $('button#btn-verif[type=submit]', $('#form-doc')).hide();
        <?php
      }
      ?>
      $('button#btn-save[type=submit]', $('#form-doc')).show();
      <?php
    } else {
      ?>
      $('input,select,textarea', $('#form-doc')).attr('disabled', true);
      $('button#btn-verif[type=submit]', $('#form-doc')).hide();
      $('button#btn-save[type=submit]', $('#form-doc')).hide();
      <?php
    }
  } else {
    ?>
    $('button#btn-verif[type=submit]', $('#form-doc')).hide();
    <?php
  }
  ?>

  $('button#btn-verif[type=submit]', $('#form-doc')).click(function(){
    $('[name=IsVerified]', $('#form-doc')).val(1);
  });
});
</script>
