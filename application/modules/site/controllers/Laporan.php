<?php
class Laporan extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    /*$ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }*/
  }

  public function pensiun() {
    $ruser = GetLoggedUser();
    $data['title'] = "Proyeksi Pegawai Pensiun";
    $data['getSKPD'] = $getSKPD = $ruser[COL_ROLEID]!=ROLEADMIN?$ruser[COL_COMPANYID]:($this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null);
    $data['getBulan'] = $getBulan = $this->input->get('Bulan')?$this->input->get('Bulan'):date('m');
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $bln = $getBulan;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($bln) && !empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and IdSkpd = ".$idOPD;
      }

      $rpegawai = $this->db
      ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_MPEGAWAI.".".COL_IDSKPD,"left")
      ->where($condSkpd)
      ->where("MONTH(DATE_ADD(PegTglLahir, INTERVAL PegBUP YEAR)) = ".$bln)
      ->where("YEAR(DATE_ADD(PegTglLahir, INTERVAL PegBUP YEAR)) = ".$tahun)
      ->order_by(COL_SKPDNAMA)
      ->order_by(COL_PEGNAMA)
      ->get(TBL_MPEGAWAI)
      ->result_array();

      $data['res'] = $rpegawai;
    }
    $this->template->load('backend', 'site/laporan/pensiun', $data);
  }

  public function iuran() {
    $ruser = GetLoggedUser();
    $data['title'] = "Proyeksi Iuran";
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getBulan'] = $getBulan = $this->input->get('Bulan')?$this->input->get('Bulan'):date('m');
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $bln = $getBulan;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($bln) && !empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and IdSkpd = ".$idOPD;
      }

      $rpegawai = $this->db
      ->select("mpegawai.*, mskpd.SkpdNama, COALESCE(mgolongan.GolonganJmlIuran, 0) + COALESCE(mjabatan.JabJmlIuran, 0) as Iuran")
      ->join(TBL_MGOLONGAN,TBL_MGOLONGAN.'.'.COL_GOLONGANKODE." = ".TBL_MPEGAWAI.".".COL_PEGGOLONGAN,"left")
      ->join(TBL_MJABATAN,TBL_MJABATAN.'.'.COL_JABNAMA." = ".TBL_MPEGAWAI.".".COL_PEGJABATAN,"left")
      ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_MPEGAWAI.".".COL_IDSKPD,"left")
      ->where($condSkpd)
      ->where("DATE_ADD(PegTglLahir, INTERVAL PegBUP YEAR) >= DATE('".date('Y-m-t', strtotime($tahun."-".$bln."-01"))."') ")
      ->order_by(COL_SKPDNAMA)
      ->order_by(COL_PEGNAMA)
      ->get(TBL_MPEGAWAI)
      ->result_array();

      $data['res'] = $rpegawai;
    }
    $this->template->load('backend', 'site/laporan/iuran', $data);
  }

  public function kas() {
    $ruser = GetLoggedUser();
    $data['title'] = "Laporan Kas";
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getBulan'] = $getBulan = $this->input->get('Bulan')?$this->input->get('Bulan'):date('m');
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $bln = $getBulan;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($bln) && !empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and tkas.IdSkpd = ".$idOPD;
      }

      $rkas = $this->db
      ->select('tkas.*, mskpd.SkpdNama, mpegawai.PegNama')
      ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKAS.".".COL_IDSKPD,"left")
      ->join(TBL_MPEGAWAI,TBL_MPEGAWAI.'.'.COL_PEGID." = ".TBL_TKAS.".".COL_IDPEGAWAI,"left")
      ->where($condSkpd)
      ->where(COL_ISVERIFIED, 1)
      ->order_by(COL_KASTANGGAL, 'desc')
      ->order_by(COL_SKPDNAMA, 'asc')
      ->get(TBL_TKAS)
      ->result_array();

      $data['res'] = $rkas;
    }
    $this->template->load('backend', 'site/laporan/kas', $data);
  }
}
