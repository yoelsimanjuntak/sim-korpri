<?php
class Pegawai extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    /*$ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }*/
  }

  public function index() {
    $data['title'] = "PEGAWAI";
    $this->template->load('backend', 'site/pegawai/index', $data);
  }

  public function index_load() {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterSkpd = !empty($_POST['filterSkpd'])?$_POST['filterSkpd']:null;

    $orderdef = array(COL_SKPDNAMA=>'asc');
    $orderables = array(null,COL_PEGNAMA,COL_PEGNIP,COL_PEGGOLONGAN,'TMTPensiun',COL_CREATEDON);
    $cols = array(COL_PEGNAMA,COL_PEGNIP,COL_PEGGOLONGAN);
    $condSkpd = "1=1";
    $condSkpdAll = "1=1";
    if(!empty($filterSkpd)) {
      $condSkpd = "IdSkpd=".$filterSkpd;
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd = "IdSkpd=".$ruser[COL_COMPANYID];
        $condSkpdAll = "IdSkpd=".$ruser[COL_COMPANYID];
      }
    }

    $queryAll = $this->db->where($condSkpdAll)->get(TBL_MPEGAWAI);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      if($order=='TMTPensiun'){

      } else {
        $this->db->order_by("YEAR(PegTglLahir)+PegBUP", $order[key($order)]);
      }
    }

    $q = $this->db
    ->select('mpegawai.*, _userinformation.Name, _userinformation.Name, mskpd.SkpdNama, YEAR(mpegawai.PegTglLahir)+mpegawai.PegBUP as TMTPensiun')
    ->where($condSkpd)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_MPEGAWAI.".".COL_CREATEDON,"left")
    ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_MPEGAWAI.".".COL_IDSKPD,"left")
    ->order_by(COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_MPEGAWAI, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<a class="btn btn-xs btn-danger btn-action" href="'.site_url('site/pegawai/delete/'.$r[COL_PEGID]).'"><i class="far fa-times-circle"></i></a>&nbsp;'.
        '<a class="btn btn-xs btn-primary btn-modal" href="'.site_url('site/pegawai/edit/'.$r[COL_PEGID]).'"><i class="far fa-search"></i></a>'
        /*'<a class="btn btn-xs btn-'.($r[COL_SKPDISAKTIF]==1?'warning':'info').' btn-action" href=""><i class="far fa-refresh"></i></a>'*/,
        $r[COL_PEGNAMA].($ruser[COL_ROLEID]==ROLEADMIN?'<br /><small class="font-italic">'.$r[COL_SKPDNAMA].'</small>':''),
        $r[COL_PEGNIP],
        $r[COL_PEGGOLONGAN],
        $r['TMTPensiun'],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_COMPANYID],
        COL_PEGNAMA=>$this->input->post(COL_PEGNAMA),
        COL_PEGNIP=>$this->input->post(COL_PEGNIP),
        COL_PEGGOLONGAN=>$this->input->post(COL_PEGGOLONGAN),
        COL_PEGJABATAN=>$this->input->post(COL_PEGJABATAN),
        COL_PEGTGLLAHIR=>$this->input->post(COL_PEGTGLLAHIR),
        COL_PEGTMT=>$this->input->post(COL_PEGTMT),
        COL_PEGBUP=>$this->input->post(COL_PEGBUP),

        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $dat[COL_PEGFILESK] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_MPEGAWAI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('site/pegawai/form');
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_PEGID, $id)->get(TBL_MPEGAWAI)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_PEGID, $id)->delete(TBL_MPEGAWAI);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      if(!empty($rdata[COL_PEGFILESK])&&file_exists(MY_UPLOADPATH.$rdata[COL_PEGFILESK])) {
        unlink(MY_UPLOADPATH.$rdata[COL_PEGFILESK]);
      }
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_PEGID, $id)
    ->get(TBL_MPEGAWAI)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_COMPANYID]!=$rdata[COL_IDSKPD]) {
      show_error('MAAF, ANDA TIDAK MEMILIKI HAK AKSES');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_COMPANYID],
        COL_PEGNAMA=>$this->input->post(COL_PEGNAMA),
        COL_PEGNIP=>$this->input->post(COL_PEGNIP),
        COL_PEGGOLONGAN=>$this->input->post(COL_PEGGOLONGAN),
        COL_PEGJABATAN=>$this->input->post(COL_PEGJABATAN),
        COL_PEGTGLLAHIR=>$this->input->post(COL_PEGTGLLAHIR),
        COL_PEGTMT=>$this->input->post(COL_PEGTMT),
        COL_PEGBUP=>$this->input->post(COL_PEGBUP),

        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file'])) {
          $config['upload_path'] = MY_UPLOADPATH;
          $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
          $config['max_size']	= 5120;
          $config['max_width']  = 4000;
          $config['max_height']  = 4000;
          $config['overwrite'] = FALSE;

          $this->load->library('upload',$config);

          $res = $this->upload->do_upload('file');
          if($res) {
            $upl = $this->upload->data();
            $dat[COL_PEGFILESK] = $upl['file_name'];
          }
        }

        $res = $this->db->where(COL_PEGID, $id)->update(TBL_MPEGAWAI, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      $data['data'] = $rdata;
      $this->load->view('site/pegawai/form', $data);
    }
  }
}
