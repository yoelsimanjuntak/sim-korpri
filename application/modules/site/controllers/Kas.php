<?php
class Kas extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    /*$ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }*/
  }

  public function index($tipe) {
    $data['title'] = "KAS ".strtoupper($tipe);
    $data['tipe'] = $tipe;
    $this->template->load('backend', 'site/kas/index', $data);
  }

  public function index_load($tipe) {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterSkpd = !empty($_POST['filterSkpd'])?$_POST['filterSkpd']:null;

    $orderdef = array(COL_SKPDNAMA=>'asc');
    $orderables = array(null,COL_KASTANGGAL,null,null,null,COL_CREATEDON);
    $cols = array(COL_KASTANGGAL,COL_KASKETERANGAN);
    $condSkpd = "1=1";

    if(!empty($filterSkpd)) {
      $condSkpd = "IdSkpd=".$filterSkpd;
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd = "IdSkpd=".$ruser[COL_COMPANYID];
      }
    }

    $queryAll = $this->db->where(COL_KASTIPE, $tipe)->where($condSkpd)->get(TBL_TKAS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tkas.*, _userinformation.Name, _userinformation.Name, mskpd.SkpdNama')
    ->where(COL_KASTIPE, $tipe)
    ->where($condSkpd)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TKAS.".".COL_CREATEDON,"left")
    ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKAS.".".COL_IDSKPD,"left")
    ->order_by(COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TKAS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<a class="btn btn-xs btn-danger btn-action '.($r[COL_ISVERIFIED]==1?'disabled':'').'" href="'.site_url('site/kas/delete/'.$r[COL_UNIQ]).'"><i class="far fa-times-circle"></i></a>&nbsp;'.
        '<a class="btn btn-xs btn-primary btn-modal" href="'.site_url('site/kas/form/'.$r[COL_UNIQ]).'"><i class="far fa-search"></i></a>'
        /*'<a class="btn btn-xs btn-'.($r[COL_SKPDISAKTIF]==1?'warning':'info').' btn-action" href=""><i class="far fa-refresh"></i></a>'*/,
        date('Y-m-d', strtotime($r[COL_KASTANGGAL])),
        $r[COL_KASKETERANGAN].($ruser=COL_ROLEID==ROLEADMIN?'<br /><small class="font-italic">'.$r[COL_SKPDNAMA].'</small>':''),
        $r[COL_KASTIPE]=='KELUAR'?number_format($r[COL_KASJUMLAH]*(-1)):number_format($r[COL_KASJUMLAH]),
        '<span class="badge bg-'.($r[COL_ISVERIFIED]==1?'success':'secondary').'">'.($r[COL_ISVERIFIED]==1?'TERVERIFIKASI':'PENDING').'</span>',
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($tipe) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_KASTIPE=>$tipe,
        COL_KASKATEGORI=>$tipe=='masuk'?'IURAN':$this->input->post(COL_KASKATEGORI),
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_COMPANYID],
        COL_IDPEGAWAI=>$tipe=='keluar'&&!empty($this->input->post(COL_IDPEGAWAI))?$this->input->post(COL_IDPEGAWAI):null,
        COL_KASTANGGAL=>$this->input->post(COL_KASTANGGAL),
        COL_KASJUMLAH=>($tipe=='masuk'?toNum($this->input->post(COL_KASJUMLAH)):-1*toNum($this->input->post(COL_KASJUMLAH))),
        COL_KASKETERANGAN=>$this->input->post(COL_KASKETERANGAN),

        COL_CREATEDON=>date('Y-m-d H:i'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $dat[COL_KASFILE] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_TKAS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['tipe'] = $tipe;
      $this->load->view('site/kas/form', $data);
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TKAS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    if($rdata[COL_ISVERIFIED]) {
      ShowJsonError('MAAF, DATA TIDAK DAPAT DIHAPUS KARENA SUDAH TERVERIFIKASI.');
      exit();
    }

    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TKAS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      if(!empty($rdata[COL_KASFILE])&&file_exists(MY_UPLOADPATH.$rdata[COL_KASFILE])) {
        unlink(MY_UPLOADPATH.$rdata[COL_KASFILE]);
      }
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function form($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TKAS)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    $tipe = strtolower($rdata[COL_KASTIPE]);
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_COMPANYID]!=$rdata[COL_IDSKPD]) {
      show_error('MAAF, ANDA TIDAK MEMILIKI HAK AKSES');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_KASKATEGORI=>$tipe=='masuk'?'IURAN':$this->input->post(COL_KASKATEGORI),
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_COMPANYID],
        COL_IDPEGAWAI=>$tipe=='keluar'&&!empty($this->input->post(COL_IDPEGAWAI))?$this->input->post(COL_IDPEGAWAI):null,
        COL_KASTANGGAL=>$this->input->post(COL_KASTANGGAL),
        COL_KASJUMLAH=>($rdata[COL_KASTIPE]=='MASUK'?toNum($this->input->post(COL_KASJUMLAH)):-1*toNum($this->input->post(COL_KASJUMLAH))),
        COL_KASKETERANGAN=>$this->input->post(COL_KASKETERANGAN),
        COL_ISVERIFIED=>$this->input->post(COL_ISVERIFIED),

        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file'])) {
          $config['upload_path'] = MY_UPLOADPATH;
          $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
          $config['max_size']	= 5120;
          $config['max_width']  = 4000;
          $config['max_height']  = 4000;
          $config['overwrite'] = FALSE;

          $this->load->library('upload',$config);

          $res = $this->upload->do_upload('file');
          if($res) {
            $upl = $this->upload->data();
            $dat[COL_KASFILE] = $upl['file_name'];
          }
        }

        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TKAS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      $data['data'] = $rdata;
      $data['tipe'] = strtolower($rdata[COL_KASTIPE]);
      $this->load->view('site/kas/form', $data);
    }
  }
}
