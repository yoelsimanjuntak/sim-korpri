<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    setlocale (LC_TIME, 'id_ID');
  }

  function now() {
    $dt = json_encode(array(
      'Day'=>date('D'),
      'Date'=>date('d'),
      'Month'=>date('M'),
      'Year'=>date('Y'),
      'Hour'=>date('H'),
      'Minute'=>date('i'),
      'Second'=>date('s')
    ));
    echo $dt;
  }

  function load_opt_pegawai() {
    $ruser = GetLoggedUser();
    $IdSkpd = $this->input->post(COL_IDSKPD);
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $IdSkpd = $ruser[COL_COMPANYID];
    }

    echo GetCombobox("select * from mpegawai where IdSkpd=".$IdSkpd." order by PegNama", COL_PEGID, array(COL_PEGNAMA, COL_PEGNIP), null, true, false, '-- PILIH PEGAWAI --');
  }

  function get_jumlah_santunan() {
    $ruser = GetLoggedUser();
    //$IdPegawai = $this->input->post(COL_IDPEGAWAI);
    $kasKategori = $this->input->get(COL_KASKATEGORI);

    /*$rpegawai = $this->db
    ->select('mpegawai.*, mgolongan.GolonganJmlIuran as IuranGol, mjabatan.JabJmlIuran as IuranJab')
    ->join(TBL_MGOLONGAN,TBL_MGOLONGAN.'.'.COL_GOLONGANKODE." = ".TBL_MPEGAWAI.".".COL_PEGGOLONGAN,"left")
    ->join(TBL_MJABATAN,TBL_MJABATAN.'.'.COL_JABNAMA." = ".TBL_MPEGAWAI.".".COL_PEGJABATAN,"left")
    ->where(COL_PEGID, $IdPegawai)
    ->get(TBL_MPEGAWAI)
    ->row_array();

    $jmlIuran = 0;
    if($rpegawai) {

    }*/

    $jmlIuran = 0;
    $rkategori = $this->db
    ->where(COL_KATNAMA, $kasKategori)
    ->get(TBL_MKATEGORI)
    ->row_array();
    if($rkategori) {
      $jmlIuran = $rkategori[COL_KATJUMLAH];
    }

    echo $jmlIuran;
  }
}
